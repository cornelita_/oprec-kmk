export const ITA = [
  {
    id: 0,
    name: 'Oliv',
    image: '/assets/ita/Oliv.jpg',
    designation: 'Penanggung Jawab',
  },
  {
    id: 1,
    name: 'Desun',
    image: '/assets/ita/Desun.jpg',
    designation: 'Wakil Penanggung Jawab',
  },
  {
    id: 2,
    name: 'Teo',
    image: '/assets/ita/Teo.jpg',
    designation: 'Wakil Penanggung Jawab',
  },
  {
    id: 3,
    name: 'Anggito',
    image: '/assets/ita/Anggito.jpg',
    designation: 'Staff',
  },
  {
    id: 4,
    name: 'Arel',
    image: '/assets/ita/Arel.jpg',
    designation: 'Staff',
  },
  {
    id: 5,
    name: 'Valent',
    image: '/assets/ita/Valent.jpg',
    designation: 'Staff',
  },
  {
    id: 6,
    name: 'David',
    image: '/assets/ita/David.jpg',
    designation: 'Staff',
  },
  {
    id: 7,
    name: 'Bhisma',
    image: '/assets/ita/Bhisma.jpg',
    designation: 'Staff',
  },
  {
    id: 8,
    name: 'Kejo',
    image: '/assets/ita/Kejo.jpg',
    designation: 'Staff',
  },
  {
    id: 9,
    name: 'Bagas',
    image: '/assets/ita/Bagas.jpg',
    designation: 'Staff',
  },
  {
    id: 10,
    name: 'Dom',
    image: '/assets/ita/Dom.jpg',
    designation: 'Staff',
  },
  {
    id: 11,
    name: 'Dimas',
    image: '/assets/ita/Dimas.jpg',
    designation: 'Staff',
  },
];
