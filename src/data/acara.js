export const ACARA = [
  {
    id: 0,
    name: 'Julian',
    image: '/assets/acara/Julian.jpg',
    designation: 'Penanggung Jawab',
  },
  {
    id: 1,
    name: 'Nadin',
    image: '/assets/acara/Nadin.jpg',
    designation: 'Wakil Penanggung Jawab',
  },
  {
    id: 2,
    name: 'Michael',
    image: '/assets/acara/Michael.jpg',
    designation: 'Staff',
  },
  {
    id: 3,
    name: 'Adeline',
    image: '/assets/acara/Adeline.jpg',
    designation: 'Staff',
  },
  {
    id: 4,
    name: 'Aga',
    image: '/assets/acara/Aga.jpg',
    designation: 'Staff',
  },
  {
    id: 5,
    name: 'Joni',
    image: '/assets/acara/Joni.jpg',
    designation: 'Staff',
  },
];
