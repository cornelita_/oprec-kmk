export const PSDM = [
  {
    id: 0,
    name: 'Samuel',
    image: '/assets/psdm/Samuel.jpg',
    designation: 'Penanggung Jawab',
  },
  {
    id: 1,
    name: 'Erlang',
    image: '/assets/psdm/Erlang.jpg',
    designation: 'Wakil Penanggung Jawab',
  },
  {
    id: 2,
    name: 'Greg',
    image: '/assets/psdm/Greg.jpg',
    designation: 'Staff',
  },
  {
    id: 3,
    name: 'Raka',
    image: '/assets/psdm/Raka.jpg',
    designation: 'Staff',
  },
  {
    id: 4,
    name: 'Cynthia',
    image: '/assets/psdm/Cynthia.jpg',
    designation: 'Staff',
  },
  {
    id: 5,
    name: 'Erick',
    image: '/assets/psdm/Erick.jpg',
    designation: 'Staff',
  },
  {
    id: 6,
    name: 'David',
    image: '/assets/psdm/David.jpg',
    designation: 'Staff',
  },
];
