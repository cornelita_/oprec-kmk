export const HPDD = [
  {
    id: 0,
    name: 'Nia',
    image: '/assets/hpdd/Nia.jpg',
    designation: 'Penanggung Jawab',
  },
  {
    id: 1,
    name: 'Pinat',
    image: '/assets/hpdd/Pinat.jpg',
    designation: 'Wakil Penanggung Jawab',
  },
  {
    id: 2,
    name: 'Rama',
    image: '/assets/hpdd/Rama.jpg',
    designation: 'Staff',
  },
  {
    id: 3,
    name: 'Jason',
    image: '/assets/hpdd/Jason.jpg',
    designation: 'Staff',
  },
];
