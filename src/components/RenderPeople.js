import React from 'react';
import {
  Card, CardImg, CardBody, CardTitle, CardSubtitle, Row,
} from 'reactstrap';
import { FadeTransform } from 'react-animation-components';

function RenderPerson({ person }) {
  const classes = (person.designation === 'Staff') ? 'mx-auto staff-img' : 'mx-auto';

  return (
    <Card className="mx-5 mb-3">
      <CardImg src={person.image} alt={person.name} className={classes} />
      <CardBody>
        <CardTitle>{person.name}</CardTitle>
        <CardSubtitle>{person.designation}</CardSubtitle>
      </CardBody>
    </Card>
  );
}

function RenderPeople({ people, type }) {
  const pjArray = (type === 'ita') ? people.slice(0, 3) : people.slice(0, 2);
  const staffArray = (type === 'ita') ? people.slice(3) : people.slice(2);

  const pj = pjArray.map((person) => (
    <FadeTransform
      key={person.id}
      in
      transformProps={{
        exitTransform: 'scale(0.5) translateY(-50%)',
      }}
    >
      <RenderPerson person={person} />
    </FadeTransform>
  ));

  const staff = staffArray.map((person) => (
    <FadeTransform
      key={person.id}
      in
      transformProps={{
        exitTransform: 'scale(0.5) translateY(-50%)',
      }}
    >
      <RenderPerson person={person} />
    </FadeTransform>
  ));

  return (
    <Row className="justify-content-center">
      <Row className="justify-content-center m-0">
        {pj}
      </Row>
      <Row className="justify-content-center m-0">
        {staff}
      </Row>
    </Row>
  );
}

export default RenderPeople;
