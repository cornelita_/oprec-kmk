import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <>
        <div className="contact-section text-center">
          <h1 className="title">Want to join us?</h1>
          <div className="mt-3">
            <img src="./assets/logo-line.png" alt="Line's logo" />
            <p>cornelita_lugita</p>
          </div>
        </div>
        <div className="footer-section">
          <h1 className="footer-text">
            <span>❤</span>
            {' '}
            Code with love
            {' '}
            <span>❤</span>
          </h1>
        </div>
      </>
    );
  }
}

export default Footer;
