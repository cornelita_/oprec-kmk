import React, { Component } from 'react';
import { Row } from 'reactstrap';
import RenderPeople from '../components/RenderPeople';

class Psdm extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  componentDidMount() {
    this.myRef.current.scrollIntoView({behavior: "smooth", block: "start", inline: "start"});
  }

  render() {
    return (
      <div ref={this.myRef} className="container division-section">
        <Row className="justify-content-center mb-5">
          <h1 className="title">Divisi PSDM</h1>
        </Row>
        <RenderPeople people={this.props.people} />
      </div>
    );
  }
}

export default Psdm;
