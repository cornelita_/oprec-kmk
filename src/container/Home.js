import React, { Component } from "react";
import { Container, Row } from "reactstrap";
import RenderPeople from "../components/RenderPeople";

class Home extends Component {
  render() {
    return (
      <Container className="division-section">
        <Row className="justify-content-center mb-5">
          <h1 className="title">Divisi Acara</h1>
        </Row>
        <RenderPeople people={this.props.people} />
      </Container>
    );
  }
}

export default Home;
