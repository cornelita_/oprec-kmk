import React, { Component } from "react";
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { ACARA } from "../data/acara";
import { HPDD } from "../data/hpdd";
import { ITA } from "../data/ita";
import { PSDM } from "../data/psdm";
import Acara from "./Acara";
import Navbar from "./Navbar/index";
import Footer from "./Footer";
import Hpdd from "./Hpdd";
import Ita from "./Ita";
import Psdm from "./Psdm";
import Jumbotron from "./Jumbotron/index";
import Home from "./Home";

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      acara: ACARA,
      psdm: PSDM,
      hpdd: HPDD,
      ita: ITA,
    };
  }

  render() {
    return (
      <div>
        <Navbar />
        <Jumbotron />
        <TransitionGroup>
          <CSSTransition
            key={this.props.location.key}
            classNames="page"
            timeout={300}
          >
            <Switch location={this.props.location}>
              <Route
                exact path="/"
                component={() => <Home people={this.state.acara} />}
              />
              <Route
                path="/acara"
                component={() => <Acara people={this.state.acara} />}
              />
              <Route
                path="/psdm"
                component={() => <Psdm people={this.state.psdm} />}
              />
              <Route
                path="/hpdd"
                component={() => <Hpdd people={this.state.hpdd} />}
              />
              <Route
                path="/ita"
                component={() => <Ita people={this.state.ita} />}
              />
              <Redirect to="/" />
            </Switch>
          </CSSTransition>
        </TransitionGroup>
        <Footer />
      </div>
    );
  }
}

export default withRouter(Main);
