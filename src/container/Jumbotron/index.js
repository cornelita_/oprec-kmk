/* eslint-disable */
import React from "react";
import { StyledJumbotron } from "./style";
import Logo from "./logo512.png";
import Backdrop from "./Group2.png";

const Jumbotron = () => {
  return (
    <StyledJumbotron background={Backdrop}>
      <div className="bigLogo">
        <img src={Logo} alt="logo" width="100%" />
      </div>
      <div className="headLine">Welcome staff of KMK CSUI 2021</div>
      <div className="tagLine">
        <i>#LuceatLuxVestra</i>
      </div>
    </StyledJumbotron>
  );
};

export default Jumbotron;
