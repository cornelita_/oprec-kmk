/* eslint-disable */
import styled from "styled-components";

export const StyledJumbotron = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: url(${(props) => props.background});
  background-size: cover;
  background-position: center;
  padding: 7.5rem 0 3.5rem 0;
  color: #000000;

  .bigLogo {
    width: 332px;
    margin-bottom: 1rem;
  }

  .tagLine {
    font-size: 2.25rem;
    margin: 1rem;
  }

  .headLine {
    font-family: "Poppins", sans-serif;
    line-height: 4.5rem;
    font-size: 3.25rem;
    font-weight: 600;
    text-align: center;
    margin: 1rem;
  }

  @media (max-width: 1024px) {
    .headLine {
      font-size: 2.25rem;
      line-height: 4rem;
    }

    .tagLine {
      font-size: 1.75rem;
    }

    .bigLogo {
      width: 296px;
    }
  }
`;
