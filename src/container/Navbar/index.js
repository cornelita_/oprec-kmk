/* eslint-disable */
import React, { useState, useEffect } from "react";
import { StyledNav } from "./style";
import { Link } from "react-router-dom";
import Logo from "./logo512.png";
import Burger from "./Group14.png";

const Navbar = (backgroundColor) => {
  const [isDesktop, setIsDesktop] = useState(true);
  const [isOpened, setIsOpened] = useState(false);

  useEffect(() => {
    setIsDesktop(window.innerWidth >= 1024);
    setIsOpened(!isDesktop && isOpened);
  }, [isDesktop, isOpened]);

  const openNav = () => {
    setIsOpened(!isOpened);
  };

  const changeWindowSize = () => {
    setIsDesktop(window.innerWidth >= 1024);
  };

  window.addEventListener("resize", changeWindowSize);

  return (
    <StyledNav background={backgroundColor} openNav={isOpened}>
      <div className="mainNav">
        <div className="logo">
          <img src={Logo} alt="logo" width="100%" />
        </div>
        {isDesktop ? (
          <div className="menu">
            <Link className="nav-link" to="/acara">
              Acara
            </Link>
            <Link className="nav-link" to="/psdm">
              PSDM
            </Link>
            <Link className="nav-link" to="/hpdd">
              HPDD
            </Link>
            <Link className="nav-link" to="/ita">
              ITA
            </Link>
          </div>
        ) : (
          <div className="menu">
            <div className="burgerButton" onClick={openNav}>
              <img src={Burger} alt="logo" width="100%" />
            </div>
          </div>
        )}
      </div>
      <div className="sideNav">
        <Link className="nav-link" to="/acara" onClick={openNav}>
          Acara
        </Link>
        <Link className="nav-link" to="/psdm" onClick={openNav}>
          PSDM
        </Link>
        <Link className="nav-link" to="/hpdd" onClick={openNav}>
          HPDD
        </Link>
        <Link className="nav-link" to="/ita" onClick={openNav}>
          ITA
        </Link>
      </div>
    </StyledNav>
  );
};

export default Navbar;
