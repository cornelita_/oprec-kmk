/* eslint-disable */
import styled from "styled-components";

export const StyledNav = styled.div`
  width: 100%;
  padding: 1rem 1.25rem 1rem 1.25rem;
  background: ${(props) =>
    props.openNav ? "rgba(0, 0, 0, 0.8)" : "rgba(0, 0, 0, 0.6)"};
  position: fixed;
  z-index: 999;

  .mainNav {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .logo {
    width: 55px;
    height: 55px;
  }

  .burgerButton {
    width: 40px;
    height: 40px;
  }

  .menu {
    display: flex;
    align-items: center;
    font-family: "Montserrat", sans-serif;
    font-weight: 600;
    font-size: 1.25rem;
    line-height: 2.5rem;
  }

  .sideNav {
    padding: 1rem 0 2rem 0;
    display: ${(props) => (props.openNav ? "flex" : "none")};
    align-items: center;
    flex-direction: column;

    .nav-link {
      line-height: 3.25rem;
      font-size: 1.5rem;
    }
  }

  .nav-link {
    color: #ffffff;
    text-decoration: none;
    width: 7rem;
    display: flex;
    justify-content: center;
  }

  .nav-link:hover {
    font-size: 1.75rem;
    font-weight: 700;
  }
`;
